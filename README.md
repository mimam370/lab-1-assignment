# Greeting Program

This is a simple Python program that greets the user by their name. The user is prompted to enter their name, and the program responds with a personalized greeting.

## Code Example

```python
def greet(name):
    return f"Hello, {name}!"

if __name__ == "__main__":
    user_name = input("Enter your name: ")
    print(greet(user_name))
